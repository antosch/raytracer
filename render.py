import time
import numbers
import vector
import objects3D
import torch as tr
import matplotlib.pyplot as plt

dtype = lambda *args, **kargs: tr.FloatTensor(*args, **kargs)

def extract(cond, x):
    if isinstance(x, numbers.Number):
        return x
    if isinstance(x, tr.Tensor):
        if x.ndim == 0:
            return x
    return x[cond]


class Renderer:
    def __init__(self, w, h, scene):
        '''

        :param w: image width
        :param h: image height
        :param scene: list with scene objects
        '''
        self.w = w
        self.h = h
        self.scene = scene

        # time rendering process
        self.t0 = time.time()

        # Q - E is view direction
        self.Q = self.__screen_cords__()

    # return Q - E normalized
    # S: Screen coordinates x0, y0, x1, y1
    def __screen_cords__(self):
        '''
        :return: tensor array with all screen coordinates
        '''
        r = float(self.w) / self.h
        S = (-1, 1 / r + .25, 1, -1 / r + .25)

        x = tr.linspace(S[0], S[2], self.w)
        x = tr.cat(self.h * [x])

        y = tr.linspace(S[1], S[3], self.h)
        temp = []
        for i in range(self.h):
            temp.append(tr.ones((), dtype=tr.float32))
            temp[i] = temp[i].new_full((1, self.w), y[i])
        y = tr.stack(temp)
        y = tr.reshape(y, (1, self.w * self.h))
        Q = vector.Vec3(x, y, 0)
        return Q

    def generate_img(self, color, verbose=False):
        '''
        :param color: array with color values for each pixel
        :return: resulting image tensor
        '''

        img_tensor = tr.cat((color.x, color.y, color.z))
        img_tensor = tr.reshape(img_tensor, (3, self.h, self.w))

        if verbose:
            print("img_tensor: ", img_tensor.shape)

        if verbose:
            print("Took", time.time() - self.t0)

        img = (img_tensor / img_tensor.max()).detach().permute(1, 2, 0)
        plt.imsave('result.png', img.numpy())
        return img_tensor


def raytrace(O, D, scene, bounce=0):
    '''
    :param O: ray origin
    :param D: ray direction
    :param scene: list of scene objects
    :param bounce: reflectiveness
    :return: array with color values for each pixel
    '''

    # find nearest intersection points with objects in scene
    distances = [dtype(s.intersect(O, D)) for s in scene]
    nearest, nearest_idx = tr.min(tr.stack(distances), dim=0)
    rgb = vector.Vec3
    color = rgb(0, 0, 0)
    ls = len(scene)

    for (s, d) in zip(scene, range(ls)):
        hit = (nearest < objects3D.FARAWAY) & (nearest_idx == d) & (nearest > objects3D.NUDGED)
        if hit.any():
            # determine color at each ray-sphere intersection point
            dc = extract(hit, nearest)
            Dc = D.extract(hit)
            Oc = O.extract(hit)
            cc = s.light(Oc, Dc, dc, scene, bounce)
            color += cc.place(hit)

    return color
