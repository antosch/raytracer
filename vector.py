import numpy as np
import render
import numbers
import torch as tr
import math

''' 3 dimensional vector with different functionalities '''

class Vec3():
    def __init__(self, x, y, z):
        if isinstance(x, tr.Tensor): x = x.float()
        if isinstance(y, tr.Tensor): y = y.float()
        if isinstance(z, tr.Tensor): z = z.float()
        (self.x, self.y, self.z) = (x, y, z)

    def __add__(self, other):
        return Vec3(self.x + other.x, self.y + other.y, self.z + other.z)

    def __sub__(self, other):
        return Vec3(self.x - other.x, self.y - other.y, self.z - other.z)

    def __mul__(self, other):
        if isinstance(other, Vec3):
            return Vec3(self.x * other.x, self.y * other.y, self.z * other.z)
        else:
            return Vec3(self.x * other, self.y * other, self.z * other)

    def __truediv__(self, v):
        if isinstance(v, Vec3):
            return Vec3(self.x / v.x, self.y / v.y, self.z / v.z)
        elif isinstance(v, numbers.Number) or isinstance(v, tr.Tensor):
            return Vec3(self.x / v, self.y / v, self.z / v)

    def __rtruediv__(self, v):
        if isinstance(v, Vec3):
            return Vec3(v.x / self.x, v.y / self.y, v.z / self.z)
        elif isinstance(v, numbers.Number) or isinstance(v, tr.Tensor):
            return Vec3(v / self.x, v / self.y, v / self.z)

    # dot product
    def dot(self, other):
        return (self.x * other.x) + (self.y * other.y) + (self.z * other.z)

    # absolute value
    def __abs__(self):
        return self.dot(self)

    # cross product
    def cross(self, other):
        return Vec3((self.y * other.z - self.z * other.y),
                    (self.z * other.x - self.x * other.z),
                    (self.x * other.y - self.y * other.x))

    # vector norm
    def norm(self):
        sqroot = tr.sqrt(abs(self)) if isinstance(abs(self), tr.Tensor) else math.sqrt(abs(self))
        if isinstance(sqroot, tr.Tensor):
            sqroot = tr.where(sqroot == 0, tr.ones(1), sqroot.float())
        else:
            sqroot = 1 if sqroot == 0 else sqroot
        return self * (1 / sqroot)

    def components(self):
        return (self.x, self.y, self.z)

    # extract values with given condition
    def extract(self, cond):
        return Vec3(render.extract(cond, self.x),
                    render.extract(cond, self.y),
                    render.extract(cond, self.z))

    def place(self, cond):
        r = Vec3(tr.zeros(cond.shape, dtype=tr.double), tr.zeros(cond.shape,  dtype=tr.double), tr.zeros(cond.shape,  dtype=tr.double))
        r.x[cond] = self.x
        r.y[cond] = self.y
        r.z[cond] = self.z
        return r

    # these functions are mostly for the box normals, may be able to delete some later on
    def matmul(self, matrix):
        if isinstance(self.x, numbers.Number):
             return array_to_vec3(np.dot(matrix, self.to_array()))
        elif isinstance(self.x, tr.Tensor):
            return array_to_vec3(tr.mm(matrix, self.to_array(), axes=([1, 0])))

    def to_array(self):
        return tr.tensor([self.x, self.y, self.z])


def array_to_vec3(array):
    return Vec3(array[0], array[1], array[2])