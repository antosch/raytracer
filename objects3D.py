import render
import vector
import torch as tr

# set constant for infinity value
FARAWAY = tr.finfo(tr.float32).max
NUDGED = 0.00001

class Sphere:
    def __init__(self, center, r, diffuse, E, L, mirror = 0.5):
        '''

        :param center: the vec3 coordinate of the spheres center point
        :param r: the sphere radius
        :param diffuse: the surface color
        :param E: camera origin
        :param L: light origin
        :param mirror: reflectiveness of the surface
        '''

        self.c = center
        self.r = r
        self.diffuse = diffuse
        self.mirror = mirror
        self.E = E
        self.L = L

    def intersect(self, O, D):
        '''
        calculates the ray-sphere intersection by solving the
        quadratic formula for the intersection points

        :param O: ray origin
        :param D: ray directions
        :return: distances of ray-sphere intersection
        '''

        b = 2 * D.dot(O - self.c)
        c = abs(self.c) + abs(O) - 2 * self.c.dot(O) - (self.r * self.r)
        disc = (b ** 2) - (4 * c)
        sq = tr.sqrt(tr.relu(disc))
        h0 = (-b - sq) / 2
        h1 = (-b + sq) / 2
        h = tr.where((h0 > 0) & (h0 < h1), h0, h1)
        pred = (disc > 0) & (h > 0)
        return tr.where(pred, h, tr.ones_like(h) * FARAWAY)

    def diffusecolor(self, hit):
        return self.diffuse

    def light(self, O, D, d, scene, bounce):
        '''
        calculates the color at the ray-sphere intersection point

        :param O: ray origin
        :param D: ray directions
        :param d: distances to ray-sphere intersection
        :param scene: list of scene objects
        :param bounce: reflective rays
        :return: color of pixels
        '''
        hit = O + D * d                       # intersection point
        N = (hit - self.c) * (1. / self.r)    # surface normal
        toL = (self.L - hit).norm()           # direction to light source
        toO = (self.E - hit).norm()           # direction to ray origin

        nudged = hit + N * NUDGED            # hit nudged to avoid self intersection errors

        # The point is not shadowed if a vector from hit to light origin has no intersections along the way
        light_distances = [s.intersect(nudged, toL) for s in scene]
        light_nearest, idx = tr.min(tr.stack(light_distances), dim=0)
        unshadowed = light_distances[scene.index(self)] == light_nearest

        # Ambient shading
        rgb = vector.Vec3
        color = rgb(0.05, 0.05, 0.05)

        # diffuse shading
        toLight = N.dot(toL)
        toLight_pos = tr.where(toLight > 0, toLight, tr.zeros_like(toLight))
        color = color + self.diffusecolor(hit) * toLight_pos * unshadowed

        # Reflection
        if bounce < 2:
            rayD = (D - N * 2 * D.dot(N)).norm()
            color += render.raytrace(nudged, rayD, scene, bounce + 1) * self.mirror

        # Blinn-Phong shading (specular)
        phong = N.dot((toL + toO).norm())
        color += rgb(1, 1, 1) * tr.pow(tr.clamp(phong, 0, 1), 50) * unshadowed

        return color

# creates a checker pattern on a sphere object
class CheckerPattern(Sphere):
    def diffusecolor(self, hit):
        checker = ((hit.x * 2).type(tr.int) % 2) == ((hit.z * 2).type(tr.int) % 2)
        return self.diffuse * checker

'''
The Box class is as of yet unfinished and does not render properly. 
It has also not yet been fully converted to work with PyTorch. We descided
to keep the code, since we might come back to fix this in the future
'''
class Box():

    def __init__(self, vmin, vmax, diffuse, E, L, mirror=0.5):
        '''

        :param vmin: minimal box coordinate
        :param vmax: maximal box coordinate
        :param diffuse: surface color
        :param E: camera origin
        :param L: light source origin
        :param mirror: reflectiveness of surface
        '''

        self.min = vmin
        self.max = vmax

        self.extent = self.max - self.min
        self.center = self.extent / 2
        self.diffuse = diffuse
        self.mirror = mirror
        self.E = E
        self.L = L

        values = self.center - self.min
        self.width = values.x * 2
        self.height = values.y * 2
        self.length = values.z * 2

        # basis vectors
        self.ax_w = vector.Vec3(1., 0., 0.)
        self.ax_h = vector.Vec3(0., 1., 0.)
        self.ax_l = vector.Vec3(0., 0., 1.)

        self.inverse_basis_matrix = tr.tensor([[self.ax_w.x, self.ax_h.x, self.ax_l.x],
                                               [self.ax_w.y, self.ax_h.y, self.ax_l.y],
                                               [self.ax_w.z, self.ax_h.z, self.ax_l.z]])

        self.basis_matrix = self.inverse_basis_matrix.T

    def intersect(self, O, D):
        '''
        intersection of ray with box planes

        :param O: ray origin
        :param D: ray direction
        :return: distances of intersection points.
        '''


        invD = 1 / D                        # multiply with inverse ray direction to avoid division by zero

        tmin = (self.min.x - O.x) * invD.x
        tmax = (self.max.x - O.x) * invD.x
        tymin = (self.min.y - O.y) * invD.y
        tymax = (self.max.y - O.y) * invD.y

        # min is minimum of max, min
        tmin = tr.where((tmax < tmin), tmax, tmin)
        tymin = tr.where((tymax < tymin), tymax, tymin)

        # case ray misses box completely: set distance to infinity
        tmin = tr.where((tmin > tymax), tr.ones_like(tmin) * FARAWAY, tmin)
        tymin = tr.where((tymin > tmax), tr.ones_like(tymin) * FARAWAY, tymin)

        # set tmin to the larger min value and tmax to the smaller max value of x and y coordinates
        tmin = tr.where((tymin > tmin), tymin, tmin)
        tmax = tr.where((tymax < tmax), tymax, tmax)

        tzmin = (self.min.z - O.z) * invD.z
        tzmax = (self.max.z - O.z) * invD.z

        # case ray misses box completeley: set distance to infinity
        tmin = tr.where((tmin > tzmax), tr.ones_like(tmin) * FARAWAY, tmin)
        tzmin = tr.where((tzmin > tmax), tr.ones_like(tzmin) * FARAWAY, tzmin)

        # set tmin to the larger of the min values and tmax to the smaller of the max values
        tmin = tr.where((tzmin > tmin), tzmin, tmin)
        tmax = tr.where((tzmax < tmax), tzmax, tmax)

        return tmin

    def diffusecolor(self, hit):
        return self.diffuse

    def get_normal(self, hit):
        P = (hit - self.center).matmul(self.basis_matrix)
        absP = vector.Vec3(1. / self.width, 1. / self.height, 1. / self.length) * tr.abs(P)
        Pmax = tr.maximum(tr.maximum(absP.x, absP.y), absP.z)
        P.x = tr.where(Pmax == absP.x, tr.sign(P.x), 0.)
        P.y = tr.where(Pmax == absP.y, tr.sign(P.y), 0.)
        P.z = tr.where(Pmax == absP.z, tr.sign(P.z), 0.)

        return P.matmul(self.inverse_basis_matrix)

    def light(self, O, D, d, scene, bounce):
        M = O + D * d               # intersection point
        N = self.get_normal(M)      # normal
        toL = (self.L - M).norm()   # direction to light
        toO = (self.E - M).norm()   # direction to ray origin

        nudged = M + N * .0001      # M nudged to avoid intersection with itself

        # determine Shadow
        light_distances = [s.intersect(nudged, toL) for s in scene]
        light_nearest, idx = tr.min(tr.stack(light_distances), dim=0)
        # seelight = 1 if tr.all(light_distances[scene.index(self)] == light_nearest) else 0
        seelight = light_distances[scene.index(self)] == light_nearest

        # Ambient
        rgb = vector.Vec3
        color = rgb(0.05, 0.05, 0.05)

        # Lambert shading (diffuse)
        toLight = N.dot(toL)

        lv = tr.where(toLight > 0, toLight, tr.zeros_like(toLight))
        color = color + self.diffusecolor(M) * lv * seelight

        # Reflection
        if bounce < 2:
            rayD = (D - N * 2 * D.dot(N)).norm()
            color += render.raytrace(nudged, rayD, scene, bounce + 1) * self.mirror

        # Blinn-Phong shading (specular)
        phong = N.dot((toL + toO).norm())
        color += rgb(1, 1, 1) * tr.pow(tr.clamp(phong, 0, 1), 50) * seelight

        return color