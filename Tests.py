import objects3D
import vector
import render
import unittest

'''Test Classes for Vec3 as well as sphere and checker pattern initiation'''


class TestVectors(unittest.TestCase):
    def setUp(self):
        self.v1 = vector.Vec3(1.0, -2.0, -2.0)
        self.v2 = vector.Vec3(3.0, 6.0, 9.0)

    def test_addition(self):
        sum1 = self.v1 + self.v2
        self.assertEqual(sum1.components(), (4.0, 4.0, 7.0))

    def test_subtraction(self):
        diff = self.v1 - self.v2
        self.assertEqual(diff.components(), (-2.0, -8.0, -11.0))

    def test_multiplication_scalar(self):
        product = self.v1 * 2
        self.assertEqual(product.components(), (2.0, -4.0, -4.0))

    def test_multiplication(self):
        product = self.v1*self.v2
        self.assertEqual(product.components(), (3.0, -12.0, -18.0))

    def test_truediv_scalar(self):
        quotient = self.v1 / 2
        self.assertEqual(quotient.components(), (0.5, -1.0, -1.0))

    def test_truediv(self):
        quotient = self.v1/self.v2
        self.assertEqual(quotient.components(), (1/3, -2/6, -2/9))

    def test_rtruediv_scalar(self):
        quotient = 2 / self.v1
        self.assertEqual(quotient.components(), (2.0, 2/-2.0, 2/-2.0))

    def test_rtruediv(self):
        quotient = self.v2 / self.v1
        self.assertEqual(quotient.components(), (3/1, 6/-2, 9/-2))

    def test_dot_product(self):
        self.assertEqual(self.v1.dot(self.v2), -27)

    def test_absolute(self):
        self.assertEqual(self.v1.__abs__(), 9.0)

    def test_cross_product(self):
        self.assertEqual(self.v1.cross(self.v2).components(), (-6.0, -15.0, 12.0))

    def test_norm(self):
        v3 = self.v1.norm()
        print(v3)
        self.assertEqual(v3.components(), (self.v1.x*(1/3), self.v1.y*(1/3), self.v1.z*(1/3)))

    def test_components(self):
        self.assertEqual(self.v1.components(), (1, -2, -2))

class TestSphereInitiation(unittest.TestCase):
    def setUp(self):
        self.L = vector.Vec3(5, 5, -10)
        self.E = vector.Vec3(0, 0.35, -.8)
        self.rgb = vector.Vec3(1, 0, 0)
        self.s = objects3D.Sphere(vector.Vec3(1, 1, 1), 0.8, self.rgb, self.E, self.L)
        self.ch = objects3D.CheckerPattern(vector.Vec3(1, 1, 1), 0.8, self.rgb, self.E, self.L)

    def test_center(self):
        self.assertEqual(self.s.c.components(), (1, 1, 1))

    def test_radius(self):
        self.assertEqual(self.s.r, 0.8)

    def test_color(self):
        self.assertEqual(self.s.diffuse, self.rgb)

    def test_E(self):
        self.assertEqual(self.s.E, self.E)

    def test_L(self):
        self.assertEqual(self.s.L, self.L)

    def test_diffusecolor(self):
        self.assertEqual(self.s.diffusecolor(self.L), self.rgb)

class CheckerPattern(unittest.TestCase):
    def setUp(self):
        self.L = vector.Vec3(5, 5, -10)
        self.E = vector.Vec3(0, 0.35, -.8)
        self.rgb = vector.Vec3(1, 0, 0)
        #self.s = objects3D.Sphere(vector.Vec3(1, 1, 1), 0.8, self.rgb, self.E, self.L)
        self.ch = objects3D.CheckerPattern(vector.Vec3(1, 1, 1), 0.8, self.rgb, self.E, self.L)

    def test_center(self):
        self.assertEqual(self.ch.c.components(), (1, 1, 1))

    def test_radius(self):
        self.assertEqual(self.ch.r, 0.8)

    def test_color(self):
        self.assertEqual(self.ch.diffuse, self.rgb)

    def test_E(self):
        self.assertEqual(self.ch.E, self.E)

    def test_L(self):
        self.assertEqual(self.ch.L, self.L)

class screenCoordinates(unittest.TestCase):
    def setUp(self):
        self.L = vector.Vec3(5, 5, -10)
        self.E = vector.Vec3(0, 0.35, -.8)
        self.rgb = vector.Vec3(1, 0, 0)
        self.s = objects3D.Sphere(vector.Vec3(1, 1, 1), 0.8, self.rgb, self.E, self.L)

        self.w = 500
        self.h = 400
        self.s = [objects3D.Sphere(vector.Vec3(1, 1, 1), 0.8, self.rgb, self.E, self.L),]
        self.r = render.Renderer(self.w, self.h, self.s)

    def test_screencoord(self):
        self.assertEqual(len(self.r.Q.x.numpy()), 400*500)
        self.assertEqual(len(self.r.Q.y.numpy()),1)
        self.assertEqual(self.r.Q.z,0)


if __name__ == "__main__":
    unittest.main(argv=['first-arg-is-ignored'], exit=False)