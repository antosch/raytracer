import vector
import objects3D
import render
import matplotlib.pyplot as plt
# lightpoint, origin for 3D objects
L = vector.Vec3(5, 5, -10)
E = vector.Vec3(0, 0.35, -1)
rgb = vector.Vec3

# create scene
scene = [
    objects3D.Sphere(vector.Vec3(.75, .1, 1), .6, rgb(0, 0, 1), E, L),
    objects3D.Sphere(vector.Vec3(-.75, .1, 2.25), .6, rgb(.5, .223, .5), E, L),
    objects3D.CheckerPattern(vector.Vec3(0, -99999.5, 0), 99999, rgb(.75, .75, .75), E, L),
    #Box(Vec3(-1.4, 3.5, 1.25), Vec3(-1.95, -1.3, 1.9), rgb(1, 0, 1), E, L),
]

w = 400
h = 300
r = render.Renderer(w, h, scene)

color = render.raytrace(E, (r.Q - E).norm(), r.scene)

img = r.generate_img(color, verbose=True)
img = (img / img.max()).detach().permute(1, 2, 0)
plt.figure()
plt.imshow(img)


